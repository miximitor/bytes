#include <iostream>
#include "bytes.hpp"

int main()
{
  extension::bytes bytes;
  bytes.push_back(1L);
  std::cout<<bytes.debug_string()<<" "<<bytes.memory_used()<<std::endl;
  bytes.push_back(2);
  std::cout<<bytes.debug_string()<<" "<<bytes.memory_used()<<std::endl;
  bytes.push_front(3L);
  std::cout<<bytes.debug_string()<<" "<<bytes.memory_used()<<std::endl;
  bytes.push_front(4L);
  std::cout<<bytes.debug_string()<<" "<<bytes.memory_used()<<std::endl;
}
