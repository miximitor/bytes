#ifndef BYTES_BYTES_HPP
#define BYTES_BYTES_HPP

#include <boost/asio/buffer.hpp>
#include <boost/assert.hpp>
#ifndef NDBUG
#include <string>
#include <cstdio>
#endif
namespace extension
{
template
  <
    typename Alloc = std::allocator<uint8_t>,
    typename Adapt = std::vector<uint8_t, Alloc>
  >
class bytes
{
public:
  
  using implement_type = Adapt;
  using iterator = typename implement_type::iterator;
  using const_iterator = typename implement_type::const_iterator;
  
  explicit bytes(std::size_t sz = 0);
  
  bytes(const bytes &other);
  
  bytes(bytes &&other) noexcept;
  
  ~bytes();
  
  bytes &operator=(bytes other) noexcept;
  
  iterator begin() noexcept;
  
  iterator end() noexcept;
  
  const_iterator begin() const noexcept;
  
  const_iterator end() const noexcept;
  
  std::size_t size() const noexcept;
  
  bool empty() const noexcept;
  
  void swap(bytes &other) noexcept;
  
  std::size_t capacity_front() const noexcept;
  
  std::size_t capacity_back() const noexcept;
  
  std::size_t memory_used() const noexcept;
  
  void reserve_front(std::size_t n);
  
  void reserve_back(std::size_t n);
  
  boost::asio::mutable_buffer prepare_front(std::size_t n);
  
  boost::asio::mutable_buffer prepare_back(std::size_t n);
  
  void commit_front(std::size_t n);
  
  void commit_back(std::size_t n);
  
  void consume_front(std::size_t n);
  
  void consume_back(std::size_t n);
  
  void clear_back();
  
  void clear_front();
  
  boost::asio::const_buffer
  view(const_iterator start, std::size_t len) const noexcept;
  
  void clear();
  
  template<typename T>
  void push_front(const T &t);
  
  template<typename T>
  void push_back(const T &t);
  
  template<typename T>
  void pop_front(T &t);
  
  template<typename T>
  void pop_back(const T &t);

#ifndef NDBUG
  std::string debug_string()
  {
    std::string str;
    for (auto i = begin_;i!=end_;i++)
    {
      char temp_hex[3];
      sprintf(temp_hex,"%2.2X",*i & 0xff);
      str.append(temp_hex,2);
    }
    return str;
  }
#endif

private:
  implement_type storage_;
  iterator begin_;
  iterator end_;

private:
  
  void adjust_space_front(std::size_t n);
  
  void adjust_space_back(std::size_t n);
  
  void copy_swap(bytes &other, std::size_t start) noexcept;
  
  void sure_space_front(std::size_t n);
  
  void sure_space_back(std::size_t n);
  
  std::pair<void *, std::size_t> get_view(
    const_iterator start,
    std::size_t pos
  ) const;
  
};


template
  <
    typename Alloc = std::allocator<uint8_t>,
    typename Adapt = std::vector<uint8_t, Alloc>
  >
struct IO
{
  bytes<Alloc, Adapt> read;
  bytes<Alloc, Adapt> write;
  
  IO(std::size_t n = 4096) : read(n), write(n)
  {};
};

template<typename Alloc, typename Adapt>
bytes<Alloc, Adapt>::bytes(std::size_t sz) :
  storage_(sz, 0x00), begin_(storage_.begin() + sz/2), end_(begin_)
{
}

template<typename Alloc, typename Adapt>
bytes<Alloc, Adapt>::bytes(const bytes &other) :
  storage_(other.storage_),
  begin_(storage_.begin() + (other.begin_ - other.storage_.begin())),
  end_(storage_.end() - (other.storage_.end() - other.end_))
{
}

template<typename Alloc, typename Adapt>
bytes<Alloc, Adapt>::bytes(bytes &&other) noexcept :
  storage_(std::move(other.storage_)),
  begin_(std::move(other.begin_)),
  end_(std::move(other.end_))
{
}

template<typename Alloc, typename Adapt>
bytes<Alloc, Adapt> &bytes<Alloc, Adapt>::operator=(bytes other) noexcept
{
  swap(other);
  return *this;
}

template<typename Alloc, typename Adapt>
typename bytes<Alloc, Adapt>::iterator bytes<Alloc, Adapt>::begin() noexcept
{
  return begin_;
}

template<typename Alloc, typename Adapt>
typename bytes<Alloc, Adapt>::iterator bytes<Alloc, Adapt>::end() noexcept
{
  return end_;
}

template<typename Alloc, typename Adapt>
typename bytes<Alloc, Adapt>::const_iterator
bytes<Alloc, Adapt>::begin() const noexcept
{
  return begin_;
}

template<typename Alloc, typename Adapt>
typename bytes<Alloc, Adapt>::const_iterator
bytes<Alloc, Adapt>::end() const noexcept
{
  return end_;
}

template<typename Alloc, typename Adapt>
std::size_t bytes<Alloc, Adapt>::size() const noexcept
{
  return end_ - begin_;
}

template<typename Alloc, typename Adapt>
bool bytes<Alloc, Adapt>::empty() const noexcept
{
  return size() == 0;
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::swap(bytes &other) noexcept
{
  std::swap(other.storage_, storage_);
  std::swap(other.begin_, begin_);
  std::swap(other.end_, end_);
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::clear_front()
{
  begin_ = end_ = storage_.begin();
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::clear_back()
{
  begin_ = end_ = storage_.end();
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::clear()
{
  auto mid = storage_.begin() + (storage_.end() - storage_.begin()) / 2;
  begin_ = end_ = mid;
}

template<typename Alloc, typename Adapt>
std::size_t bytes<Alloc, Adapt>::capacity_front() const noexcept
{
  return begin_ - storage_.begin();
}

template<typename Alloc, typename Adapt>
std::size_t bytes<Alloc, Adapt>::capacity_back() const noexcept
{
  return storage_.end() - end_;
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::adjust_space_front(std::size_t n)
{
  bytes other(n + size() + capacity_back());
  copy_swap(other, n);
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::adjust_space_back(std::size_t n)
{
  bytes other(capacity_front() + size() + n);
  copy_swap(other, capacity_front());
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::copy_swap(bytes &other, std::size_t start) noexcept
{
  std::copy(begin_, end_, other.storage_.begin() + start);
  other.begin_ = other.storage_.begin() + start;
  other.end_ = other.begin_ + size();
  swap(other);
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::reserve_front(std::size_t n)
{
  if ( n > capacity_front())
  {
    adjust_space_front(n);
  }
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::reserve_back(std::size_t n)
{
  if ( n > capacity_back())
  {
    adjust_space_back(n);
  }
}

template<typename Alloc, typename Adapt>
boost::asio::mutable_buffer bytes<Alloc, Adapt>::prepare_front(std::size_t n)
{
  sure_space_front(n);
  auto[_start, _pos] = get_view(begin_ - n, n);
  return boost::asio::mutable_buffer(_start, _pos);
}


template<typename Alloc, typename Adapt>
boost::asio::mutable_buffer bytes<Alloc, Adapt>::prepare_back(std::size_t n)
{
  sure_space_back(n);
  auto[_start, _pos] = get_view(end_, n);
  return boost::asio::mutable_buffer(_start, _pos);
}


template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::sure_space_front(std::size_t n)
{
  if ( capacity_front() >= n )
    return;
  auto new_cap_front = (capacity_front() + n + size()) * 2;
  adjust_space_front(new_cap_front);
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::sure_space_back(std::size_t n)
{
  if ( capacity_back() >= n )
    return;
  auto new_cap_back = (size() + capacity_back() + n) * 2;
  adjust_space_back(new_cap_back);
}

template<typename Alloc, typename Adapt>
std::pair<void *, std::size_t>
bytes<Alloc, Adapt>::get_view(const_iterator start, std::size_t pos) const
{
  void *ptr = reinterpret_cast<void *>(
    const_cast<typename implement_type::pointer>(
      storage_.data() + (start - storage_.begin())
    ));
  return {ptr, pos};
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::commit_front(std::size_t n)
{
  BOOST_ASSERT(begin_ - n >= storage_.begin());
  begin_ -= n;
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::commit_back(std::size_t n)
{
  BOOST_ASSERT(end_ + n <= storage_.end());
  end_ += n;
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::consume_front(std::size_t n)
{
  BOOST_ASSERT(begin_ + n <= end_);
  begin_ += n;
}

template<typename Alloc, typename Adapt>
void bytes<Alloc, Adapt>::consume_back(std::size_t n)
{
  BOOST_ASSERT(end_ - n >= begin_);
  end_ -= n;
}

template<typename Alloc, typename Adapt>
boost::asio::const_buffer
bytes<Alloc, Adapt>::view(const_iterator start, std::size_t len) const noexcept
{
  BOOST_ASSERT(start >= begin());
  BOOST_ASSERT(start + len <= end());
  auto[_start, _pos] = get_view(start, len);
  return const_buffer(_start, _pos);
}

template<typename Alloc, typename Adapt>
template<typename T>
void bytes<Alloc, Adapt>::push_front(const T &t)
{
  static_assert(std::is_trivial_v<T>, "push a trivial type");
  auto memory = prepare_front(sizeof(T));
  memcpy(memory.data(), &t, memory.size());
  commit_front(memory.size());
}

template<typename Alloc, typename Adapt>
template<typename T>
void bytes<Alloc, Adapt>::push_back(const T &t)
{
  static_assert(std::is_trivial_v<T>, "push a trivial type");
  auto memory = prepare_back(sizeof(T));
  memcpy(memory.data(), &t, memory.size());
  commit_back(memory.size());
}

template<typename Alloc, typename Adapt>
template<typename T>
void bytes<Alloc, Adapt>::pop_front(T &t)
{
  static_assert(std::is_trivial_v<T>, "pop a trivial type");
  auto memory = view(begin_, sizeof(T));
  memcpy(&t, memory.data(), memory.size());
  consume_front(sizeof(T));
}

template<typename Alloc, typename Adapt>
template<typename T>
void bytes<Alloc, Adapt>::pop_back(const T &t)
{
  static_assert(std::is_trivial_v<T>, "pop a trivial type");
  auto memory = view(end_- sizeof(T), sizeof(T));
  memcpy(&t, memory.data(), memory.size());
  consume_back(sizeof(T));
}

template<typename Alloc, typename Adapt>
std::size_t bytes<Alloc, Adapt>::memory_used() const noexcept
{
  return storage_.size();
}


template<typename Alloc, typename Adapt>
bytes<Alloc, Adapt>::~bytes() = default;


}

#endif //BYTES_BYTES_HPP
